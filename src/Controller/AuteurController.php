<?php

namespace App\Controller;

use App\Repository\AuteurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuteurController extends AbstractController
{
    #[Route('/auteurs', name: 'liste_auteurs')]
    public function listeAuteurs(AuteurRepository $auteurRepository): Response
    {
        // Récupérer tous les auteurs depuis le repository
        $auteurs = $auteurRepository->findAll();

        dump($auteurs);

        return $this->render('auteur/list.html.twig', [
            'auteurs' => $auteurs,
        ]);
    }
}
