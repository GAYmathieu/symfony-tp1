<?php

// src/Controller/UserController.php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Form\AuteurFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserController extends AbstractController
{

    //accéder aux informations de son compte
    #[Route('/user/data', name: 'user_data')]
    public function userData(): Response
    {

        $user = $this->getUser();

        if (!$user) {

            throw $this->createAccessDeniedException('User not authenticated');
        }

        return $this->render('user/infoUser.html.twig', [
            'user' => $user,
        ]);
    }

    //mettre à jour ses informations
    #[Route('/updateUser/{id}', name: 'update_user')]
    public function userUpdateData(User $id, UserPasswordHasherInterface $userPasswordHasher, Request $request, EntityManagerInterface $manager): Response
    {

        $form = $this->createForm(UserType::class, $id);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('password')->getData()
                )
            );
            $manager->persist($user);
            $manager->flush();
            $this->addFlash('reussit', "modifié");
        }


        return $this->render(
            'user/userUpdate.html.twig',
            [
                'form' => $form
            ]
        );
    }

   /* #[Route('/showUsers', name: 'show_users')]
    public function showUsers(UserRepository $userRepository): Response
    {
        $users = $userRepository->findAll();

        return $this->render('main/listAll.html.twig', [
            'users' => $users,
        ]);
    }*/


    // Voir la liste des utilisateurs pour les Admins
    #[Route('/showUsers', name: 'show_users')]
    #[IsGranted("ROLE_ADMIN,", message:"Access Denied. You must be an administrator.")]
    public function accessAdminUsers(UserRepository $userRepository): Response
    {
        if (!$this->isGranted("ROLE_ADMIN")) {
            throw new AccessDeniedException("Access Denied. You must be an administrator.");
        }

        $users = $userRepository->findAll();

        return $this->render('main/listUsers.html.twig', [
            'users' => $users,
        ]);
    }
}
