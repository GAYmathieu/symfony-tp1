<?php

// src/Controller/MainController.php
namespace App\Controller;


use App\Entity\Auteur;
use App\Form\AuteurFormType;
use App\Repository\TagRepository;
use App\Repository\AuteurRepository;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{


    //afficher les données des tables auteur, articles et tags
    #[Route('/showData', name: 'show_data')]
    public function showData(AuteurRepository $auteurRepository, ArticleRepository $articleRepository, TagRepository $tagRepository, EntityManagerInterface $entityManager): Response
    {
        $auteurs = $auteurRepository->findAll();
        $articles = $articleRepository->findAll();
        $tags = $tagRepository->findAll();



        return $this->render('main/listAll.html.twig', [
            'auteurs' => $auteurs,
            'articles' => $articles,
            'tags' => $tags,
        ]);
    }


    //update auteur
    #[Route('/updateAuteur/{id}', name: 'update_auteur')]
    public function updateAuteur(Auteur $id, Request $request, EntityManagerInterface $manager): Response
    {
       
        $form = $this->createForm(AuteurFormType::class, $id);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $auteur = $form->getData();
            $manager->persist($auteur);
            $manager->flush();
            $this->addFlash('reussit', "modifié");
        }


        return $this->render(
            'auteur/update.html.twig',
            [
                'form' => $form
            ]
        );
    }

    //supprimer auteur
    #[Route('/deleteAuteur/{id}', name: 'delete_auteur')]
    public function deleteAuteur(int $id, AuteurRepository $auteurRepository, EntityManagerInterface $manager): Response
    {
        $auteur = $auteurRepository->find($id);

        if (!$auteur) {
            $this->addFlash('error', 'Auteur not found');
            return $this->redirectToRoute('show_data');
        }

        $manager->remove($auteur);
        $manager->flush();

        $this->addFlash('success', 'Auteur supprimé');
        return $this->redirectToRoute('show_data');
    }

    #[Route('/addAuteur', name: 'add_auteur')]
    public function addAuteur(Request $request, EntityManagerInterface $manager): Response
    {
        $auteur = new Auteur();
        $form = $this->createForm(AuteurFormType::class, $auteur);
    
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $auteur = $form->getData();
            $manager->persist($auteur);
            $manager->flush();
            $this->addFlash('reussit', "ajouté");
    
            return $this->redirectToRoute('ShowData');
        }
    
        return $this->render(
            'auteur/addAuteur.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
    
}
