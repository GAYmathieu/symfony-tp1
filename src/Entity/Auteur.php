<?php

namespace App\Entity;

use App\Repository\AuteurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AuteurRepository::class)]
class Auteur
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nom = null;

    #[ORM\OneToMany(mappedBy: 'auteur', targetEntity: Article::class)]
    private Collection $liste_article;

    public function __construct()
    {
        $this->liste_article = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection<int, article>
     */
    public function getListeArticle(): Collection
    {
        return $this->liste_article;
    }

    public function addListeArticle(article $listeArticle): static
    {
        if (!$this->liste_article->contains($listeArticle)) {
            $this->liste_article->add($listeArticle);
            $listeArticle->setAuteur($this);
        }

        return $this;
    }

    public function removeListeArticle(article $listeArticle): static
    {
        if ($this->liste_article->removeElement($listeArticle)) {
            // set the owning side to null (unless already changed)
            if ($listeArticle->getAuteur() === $this) {
                $listeArticle->setAuteur(null);
            }
        }

        return $this;
    }
}
